# Ansible role DataSync

Role to synchronize data between active and passive servers in my self-hosting context.

The data (files and database) is first sent from the active server to a backup server, then sent to the passive servers.

The playbooks must target the backup server. The role then delegates some tasks to the active and passive servers, which must be in groups called `{{datasync_servicename}}_active` and `{{datasync_servicename}}_passive` (by default, it can be overriden with `datasync_active_rolename` and `datasync_passive_rolename` variables)

Example of use of this role to sync data for a nextcloud instance :
```
- hosts: backup
  become: yes
  gather_facts: no
  tasks:
  - name: sync data
    import_role:
      name: datasync
    vars:
      datasync_servicename: nextcloud
      datasync_database_encoding: utf8mb4
```

Another example for Tiny Tiny RSS (using a PostgreSQL database, and for which I override the directory) :
```
- hosts: backup
  become: yes
  gather_facts: no
  tasks:
  - name: sync data
    import_role:
      name: datasync
    vars:
      datasync_servicename: ttrss
      datasync_database_type: postgresql
      datasync_files_subdir_to_sync: "{{datasync_files_dir_to_sync}}/tt-rss"
      datasync_backup_files_subdir: "{{datasync_backup_files_dir}}/tt-rss"
```